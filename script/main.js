const menuOurService = document.querySelector('.nav-menu-our-service'),
      btnOurService = document.querySelectorAll('.btn-our-service'),
      contentOurService = document.querySelectorAll('div.content-our-service');

menuOurService.addEventListener("click", () => {
    btnOurService.forEach((title) => {
        title.classList.remove('active-our-service-btn');
    });

    let dataMenuEvent = event.target.getAttribute('data-paragraph');

    contentOurService.forEach((p) => {
        console.log(p.getAttribute('data-paragraph'));
        if ( p.getAttribute('data-paragraph') === dataMenuEvent ) {
            event.target.classList.add('active-our-service-btn');
            p.classList.add('active-our-service-cont');
            p.classList.remove('not-active')

        } else {
            p.classList.add('not-active');
            p.classList.remove('active-our-service-cont');
        }
    });
});

//////////////////Our Amazing Work/////////////////
const menuAmazing = document.querySelector('.nav-menu-our-amazing'),
      btnAmazing = document.querySelectorAll('.btn-our-amazing'),
      contentAmazing = document.querySelectorAll('.gallery-amazing-content');

menuAmazing.addEventListener("click", () => {
    btnAmazing.forEach((title) => {
        title.classList.remove('btn-our-amazing-active');
    });
    let dataMenuEvent = event.target.getAttribute('data-category');

    contentAmazing.forEach((p) => {
        if ( p.getAttribute('data-category') === dataMenuEvent ) {
            event.target.classList.add('btn-our-amazing-active');
            p.classList.add('gallery-amazing-content');
            p.classList.remove('not-active')
        } else if (dataMenuEvent === 'all') {
            console.log(dataMenuEvent);
            event.target.classList.add('btn-our-amazing-active');
            contentAmazing.forEach((p) => {
                p.classList.add('gallery-amazing-content')
                p.classList.remove('not-active')
            })
        }
         else {
            p.classList.add('not-active');
            p.classList.remove('gallery-amazing-content');
        }
    });
});



///////////////////LoadMore
const loadMoreBtn = $('#load_more_amazing');

$('.nav-menu-our-amazing').click(()=>{
    loadMoreBtn.show().html('<i class="fa fa-plus"></i>Load More');
});

loadMoreBtn.click ( () => {
        loadMoreBtn.html('<i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i>\n' +
            '<span class="sr-only">Загрузка...</span>');
        setTimeout( () => {
            $('.gallery-amazing-load-12-cont').removeClass('not-active');
            loadMoreBtn.hide()
        }, 3000);
    });

///////////////////Break News//////////////////////
////////newsActive
$('.news-item').on("mouseover", () => {
    $(event.currentTarget).find('.news-item-label-title').css( {'color' : '#18CFAB'} );
    $(event.currentTarget).find('.news-date-cont').css( {'background-color' : '#18CFAB'} )
});

////////newsNotActive
$('.news-item').on("mouseout", () => {
    $(event.currentTarget).find('.news-item-label-title').css( {'color' : '#717171'} );
    $(event.currentTarget).find('.news-date-cont').css( {'background-color' : '#203E38'} )
});

//////////////////People Say//////////////////////
////////clickBtnPeopleSay
$('.people-say-btn').on('click', () => {
    $(event.target).siblings('.say-active')
                   .removeClass('say-active');
    $('.people-say').siblings('.people-say')
                    .addClass('not-active')
                    .finish();
    let dataMenuEvent = event.target.getAttribute('data-person');
    $('.people-say').each((i, cont) => {
        if ( cont.getAttribute('data-person') === dataMenuEvent ) {
            $(event.target).addClass('say-active');
            $(cont).fadeIn(1500);
            $(cont).removeClass('not-active');
        } else {
            $(cont).addClass('not-active');
            $(cont).fadeOut(0);
        }
    })
});
////////nextIconPerson
    $('.next-btn').on('click',  () => {
    let activeSayPerson = $('.people-say.active'),
       activePerson = $('.say-active'),
       activePersonIndex = activePerson.index();

        $(activeSayPerson).finish();
    if (activePersonIndex === $('.people-say-btn').length-1) {
        activePersonIndex = 0;
        activeSayPerson.fadeOut(0)
                       .addClass('not-active');
        activePerson.removeClass('say-active');
        $('.people-say').eq(activePersonIndex)
                        .fadeIn(1500)
                        .removeClass('not-active')
                        .addClass('active');
        $('.people-say-btn').eq(activePersonIndex)
                            .addClass('say-active');
    } else {
        activeSayPerson.fadeOut(0).addClass('not-active');
        activePerson.removeClass('say-active');
        $('.people-say').eq(activePersonIndex+1)
                        .fadeIn(1500).removeClass('not-active')
                        .addClass('active');
        $('.people-say-btn').eq(activePersonIndex+1)
                            .addClass('say-active');
    }
});

////////previousIconPerson
    $('.prev-btn').on('click', function () {
        let activeSayPerson = $('.people-say.active'),
            activePerson = $('.say-active'),
            activePersonIndex = activePerson.index();

            $(activeSayPerson).finish();
        if (activePersonIndex === 0) {
            activePersonIndex = $('.people-say-btn').length-1;
            activeSayPerson.fadeOut(0).addClass('not-active');
            activePerson.removeClass('say-active');
            $('.people-say').eq(activePersonIndex).removeClass('not-active')
                .addClass('active').fadeIn(1000);
            $('.people-say-btn').eq(activePersonIndex).fadeIn(1500).addClass('say-active').fadeIn(1000);
        } else {
            activeSayPerson.fadeOut(0).addClass('not-active');
            activePerson.removeClass('say-active');
            $('.people-say').eq(activePersonIndex-1).fadeIn(1500)
                                                    .removeClass('not-active')
                                                    .addClass('active');
            $('.people-say-btn').eq(activePersonIndex-1).addClass('say-active');
        }

    });
